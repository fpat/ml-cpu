FROM tensorflow/tensorflow:latest-py3

## ensure locale is set during build
ENV LANG C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN pip uninstall --yes tensorflow && \
    pip install tensorflow==2.2.0 && \
    pip install keras && \
    pip install uproot && \
    pip install jupyter && \
    pip install jupyterhub && \
    pip install jupyterlab && \
    pip install matplotlib && \
    pip install seaborn && \
    pip install hep_ml && \
    pip install sklearn && \
    pip install tables && \
    pip install papermill pydot Pillow &&\
    pip install tflearn &&\
    pip install xgboost &&\
    pip install h5py
    

    

RUN apt-get update && \
    apt-get install -y git debconf-utils && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    apt-get install -y krb5-user && \
    apt-get install -y vim less screen graphviz python3-tk wget && \
    apt-get install -y evince
    
## run jupyter notebook by default unless a command is specified
CMD ["jupyter", "notebook", "--port", "33333", "--no-browser"]
