# ML-Container

This is an exemplary container for machine learning purposes on GPUs

to run on pcaz001 simply enter command.

```
singularity exec docker://gitlab-registry.cern.ch/fpat/ml-cpu/ml-cpu:latest bash
```

If you want to extend the image you can simply fork this repository and add additional packages in the [`Dockerfile`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-images/ml-gpu/blob/master/Dockerfile) and possibly create a merge request.
with the flag `-B` you can add other directories to the container if needed.

